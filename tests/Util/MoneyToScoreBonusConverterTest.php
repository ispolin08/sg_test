<?php

namespace App\Util;

use App\Entity\MoneyBonus;
use PHPUnit\Framework\TestCase;

class MoneyToScoreBonusConverterTest extends TestCase
{
    public function testConvert(): void
    {
        $bonus = new MoneyBonus();
        $bonus->setAmount(100);

        $converter = new MoneyToScoreBonusConverter(0.5);
        $scoreBonus = $converter->convert($bonus);

        $this->assertEquals(50, $scoreBonus->getAmount());
        $this->assertNotNull($bonus->getProcessedAt());
    }
}
