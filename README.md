# Init

Prepare env variables
    
    cp .env .env.local

Setup `DATABASE_URL` variable

Prepare database structure. 

    bin/console doctrine:database:create
    bin/console d:s:u --force 
    bin/console doctrine:fixtures:load

Run server

    symfony server:start -d
    

