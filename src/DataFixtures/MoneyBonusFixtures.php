<?php

namespace App\DataFixtures;

use App\Manager\MoneyBonusManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MoneyBonusFixtures extends Fixture
{
    private MoneyBonusManager $moneyBonusManager;

    public function __construct(MoneyBonusManager $moneyBonusManager)
    {
        $this->moneyBonusManager = $moneyBonusManager;
    }

    public function load(ObjectManager $manager): void
    {
        $manager->persist($this->moneyBonusManager->generate());
        $manager->flush();
    }
}
