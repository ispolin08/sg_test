<?php

namespace App\DataFixtures;

use App\Manager\ScoreBonusManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ScoreBonusFixtures extends Fixture
{
    private ScoreBonusManager $scoreBonusManager;

    public function __construct(ScoreBonusManager $scoreBonusManager)
    {
        $this->scoreBonusManager = $scoreBonusManager;
    }

    public function load(ObjectManager $manager): void
    {
        $manager->persist($this->scoreBonusManager->generate());
        $manager->flush();
    }
}
