<?php

namespace App\DataFixtures;

use App\Manager\ItemBonusManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ItemBonusFixtures extends Fixture
{

    private ItemBonusManager $itemBonusManager;

    public function __construct(ItemBonusManager $itemBonusManager)
    {
        $this->itemBonusManager = $itemBonusManager;
    }

    public function load(ObjectManager $manager): void
    {
        $manager->persist($this->itemBonusManager->generate());
        $manager->flush();
    }
}
