<?php

namespace App\DataFixtures;

use App\Entity\BonusItem;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class BonusItemFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $bonusItemsData = [
            [
                'name' => 'Pen',
            ],
        ];

        foreach ($bonusItemsData as $bonusItemData) {
            $bonusItem = new BonusItem();
            $bonusItem->setName($bonusItemData['name']);
            $manager->persist($bonusItem);
        }

        $manager->flush();
    }
}
