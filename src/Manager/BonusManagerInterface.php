<?php

namespace App\Manager;

interface BonusManagerInterface
{
    public function generate(): object;

    public function isAvailable(): bool;
}
