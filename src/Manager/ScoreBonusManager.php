<?php

namespace App\Manager;

use App\Entity\ScoreBonus;

class ScoreBonusManager implements BonusManagerInterface
{
    private array $options = [
        'from' => 100,
        'to' => 300,
    ];

    public function generate(): ScoreBonus
    {
        $bonus = new ScoreBonus();
        $bonus->setAmount(random_int($this->options['from'], $this->options['to']));

        return $bonus;
    }

    public function isAvailable(): bool
    {
        return true;
    }
}
