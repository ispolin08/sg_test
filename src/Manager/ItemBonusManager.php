<?php

namespace App\Manager;

use App\Entity\ItemBonus;
use App\Repository\BonusItemRepository;

class ItemBonusManager implements BonusManagerInterface
{
    private BonusItemRepository $bonusItemRepository;

    public function __construct(BonusItemRepository $bonusItemRepository)
    {
        $this->bonusItemRepository = $bonusItemRepository;
    }

    public function generate(): ItemBonus
    {
        $bonusItem = $this->bonusItemRepository->findRandomAvailable();

        if (!$bonusItem) {
            throw new \Exception('No available items');
        }

        $bonus = new ItemBonus();
        // TODO set $bonusItem to $bonus

        return $bonus;
    }

    public function isAvailable(): bool
    {
        return $this->bonusItemRepository->isAvailable();
    }
}
