<?php

namespace App\Manager;

use App\Entity\MoneyBonus;
use App\Repository\MoneyBonusRepository;
use App\Util\BankApi;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class MoneyBonusManager implements BonusManagerInterface
{
    private MoneyBonusRepository $moneyBonusRepository;
    private LoggerInterface $logger;
    private EntityManagerInterface $em;

    private BankApi $bankApi;

    private array $options = [
        'from' => 100,
        'to' => 300,
    ];

    public function __construct(
        MoneyBonusRepository $moneyBonusRepository,
        BankApi $bankApi,
        LoggerInterface $logger,
        EntityManagerInterface $em
    ) {
        $this->bankApi = $bankApi;
        $this->moneyBonusRepository = $moneyBonusRepository;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function generate(): MoneyBonus
    {
        $bonus = new MoneyBonus();
        $bonus->setAmount(random_int($this->options['from'], $this->options['to']));

        return $bonus;
    }

    public function isAvailable(): bool
    {
        // TODO Implement logic
        return true;
    }

    public function processBatch(int $limit = 10): void
    {
        $bonuses = $this->moneyBonusRepository->findUnprocessed($limit);
        $this->logger->notice('{count} bonuses loaded', ['count' => count($bonuses)]);

        foreach ($bonuses as $bonus) {
            $this->logger->notice('Processing bonus {id}', ['id' => $bonus->getId()]);

            try {
                $this->processBonus($bonus);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }

    private function processBonus(MoneyBonus $bonus): void
    {
        $this->bankApi->sendBonus($bonus->getAmount(), $bonus->getUserAccount());
        $bonus->setProcessedAt(new \DateTime());
        $this->em->flush();
    }
}
