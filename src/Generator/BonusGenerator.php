<?php

namespace App\Generator;

use App\Manager\BonusManagerInterface;
use Doctrine\ORM\EntityManagerInterface;

class BonusGenerator
{
    /** @var BonusManagerInterface[] */
    private array $bonusManagers;

    private EntityManagerInterface $em;

    public function __construct(
        iterable $bonusProcessors,
        EntityManagerInterface $em
    ) {
        $this->bonusManagers = iterator_to_array($bonusProcessors);
        $this->em = $em;
    }

    public function generate(): object
    {
        $available = [];
        foreach ($this->bonusManagers as $bonusManager) {
            if ($bonusManager->isAvailable()) {
                $available[] = $bonusManager;
            }
        }

        $bonus = $available[random_int(0, count($available) - 1)]->generate();
        $this->em->persist($bonus);
        $this->em->flush();

        return $bonus;
    }
}
