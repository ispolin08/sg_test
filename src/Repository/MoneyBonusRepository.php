<?php

namespace App\Repository;

use App\Entity\MoneyBonus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MoneyBonusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MoneyBonus::class);
    }

    /**
     * @return iterable<MoneyBonus>
     */
    public function findUnprocessed(int $limit = 10): iterable
    {
        return $this->findBy(['processedAt' => null], ['id' => 'asc'], $limit);
    }
}
