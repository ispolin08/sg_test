<?php

namespace App\Repository;

use App\Entity\BonusItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class BonusItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BonusItem::class);
    }

    public function findRandomAvailable(): ?BonusItem
    {
        return $this->findOneBy([]);
    }

    public function isAvailable(): bool
    {
        return (bool) random_int(0, 1);
    }
}
