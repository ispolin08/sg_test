<?php

namespace App\Command;

use App\Manager\MoneyBonusManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class ProcessMoneyBonusCommand extends Command
{
    protected static $defaultName = 'bonus:process:money';

    private MoneyBonusManager $bonusManager;

    public function __construct(
        MoneyBonusManager $bonusManager
    ) {
        parent::__construct();
        $this->bonusManager = $bonusManager;
    }

    protected function configure(): void
    {
        $this
            ->addOption('limit', null, InputOption::VALUE_REQUIRED, 'Limit of bonuses', 1)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->bonusManager->processBatch((int) $input->getOption('limit'));

        return 0;
    }
}
