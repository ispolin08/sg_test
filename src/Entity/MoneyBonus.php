<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class MoneyBonus
{
    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected int $id;


    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    protected float $amount;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected \DateTime $processedAt;

    public function getProcessedAt(): \DateTime
    {
        return $this->processedAt;
    }

    public function setProcessedAt(\DateTime $processedAt): void
    {
        $this->processedAt = $processedAt;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    // TODO manage over relation to UserProfile info
    public function getUserAccount(): string
    {
        return '1233122123123';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
