<?php

namespace App\Controller;

use App\Generator\BonusGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig', []);
    }

    /**
     * @Route("/generate", name="generate")
     */
    public function generate(BonusGenerator $generator): Response
    {
        $bonus = $generator->generate();

        return $this->render('default/generate.html.twig', [
            'bonus' => $bonus,
        ]);
    }
}
