<?php

namespace App\Util;

use App\Entity\MoneyBonus;
use App\Entity\ScoreBonus;

class MoneyToScoreBonusConverter
{
    protected float $delta;

    public function __construct(float $delta = 0.5)
    {
        $this->delta = $delta;
    }

    public function convert(MoneyBonus $bonus): ScoreBonus
    {
        $bonus->setProcessedAt(new \DateTime());
        // TODO Log or mark $bonus as converted to new bonus

        $scoreBonus = new ScoreBonus();
        $scoreBonus->setAmount((int) ($this->delta * $bonus->getAmount()));

        return $scoreBonus;
    }
}
